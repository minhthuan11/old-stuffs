## For first time run project
-   Create .env file
-   Run command:
    +   npm i
    +   composer update
    +   php artisan key:generate
    +   php artisan storage:link
    +   php artisan migrate (--seed for fake data)
## To run project
-   php artisan serve
 


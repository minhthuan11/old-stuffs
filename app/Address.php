<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Address extends Model
{
    protected $fillable = [
        'user_id','street', 'ward','city','province'
    ];

    public static function findAddressbyUserId($id) {
        return DB::table('addresses')
            ->where('user_id',$id)
            ->first();
    }

    public static function findAddressesbyUserId($id) {
        return DB::table('addresses')
            ->where('user_id',$id)
            ->get();
    }

    public static function findAddressesbyProvince($province) {
        return DB::table('addresses')
            ->where('province', $province)
            ->get();
    }

    public static function findAddressbyId($id) {
        return DB::table('addresses')->find($id);
    }

    public static function store($request)
    {
        return DB::table('addresses')->insert([
            'user_id' => Auth::user()->id,
            'street' => $request['street'],
            'ward' => $request['ward'],
            'district' => $request['district'],
            'province' => $request['province'],
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }

    public static function updateAddress($request)
    {
        return DB::table('addresses')
            ->where('user_id', Auth::user()->id)
            ->where('id', $request['id'])
            ->update([
                'street' => $request['street'],
                'ward' => $request['ward'],
                'district' => $request['district'],
                'province' => $request['province'],
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
    }

    public static function deleteAddress($id) {
        return DB::table('addresses')->where('id','=',$id)->delete();
    }

    public static function getProvinceVN() {
        return DB::table('province')->get();
    }

}

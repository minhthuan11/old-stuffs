<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $fillable = [
        'name', 'imgUrl'
    ];

    public static function getCategories($limit = null)
    {
            return DB::table('categories')
                ->orderBy('id', 'desc')
                ->get();
    }

    public static function getAllCategory($limit = null) {
        if ($limit) {
            return DB::table('categories')
                ->orderBy('id', 'asc')
                ->limit($limit)
                ->get()
            ;
        } else {
            return DB::table('categories')
                ->orderBy('id', 'desc')
                ->simplePaginate(14);
        }
    }

    public static function getCategorybyId($id) {
        return DB::table('categories')->find($id);
    }
}

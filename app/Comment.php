<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Comment extends Model
{
    protected $fillable = [
        'buyer_id', 'product_id', 'buyerRating' , 'title' ,  'content'
    ];

    public static function store($request) {
        if( isset($request['buyerRating']) ) {
            return DB::table('comments')->insertGetId([
                'product_id' => $request['product_id'],
                'buyer_id' => Auth::user()->id,
                'buyerRating' => $request['buyerRating'],
                'title' => $request['title'],
                'content' => $request['content'],
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        } else {
            return DB::table('comments')->insertGetId([
                'product_id' => $request['product_id'],
                'buyer_id' => Auth::user()->id,
                'title' => $request['title'],
                'content' => $request['content'],
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        }

    }

    public static function getCommentsbyProductId($id) {
        return DB::table('comments')
            ->where('product_id', $id)
            ->get();
    }
}

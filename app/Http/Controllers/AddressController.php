<?php

namespace App\Http\Controllers;

use App\Address;
use App\Category;
use App\Image;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AddressController extends Controller
{
    public function showIndexPage()
    {
        $loginUser = Auth::user();
        $addresses = Address::findAddressesbyUserId($loginUser->id);
        return view('pages.addressList',
            [
                'loginUser' => $loginUser,
                'addresses' => $addresses,
            ]);
    }

    public function showFormPage(Request $request)
    {
        $loginUser = Auth::user();
        $addressInfo = Address::findAddressbyId($request->id) ?? null ;
        $provinces = Address::getProvinceVN();
        return view('pages.addressForm',
            [
                'loginUser' => $loginUser,
                'addressInfo' => $addressInfo,
                'provinces' => $provinces,
            ]);
    }

    public function storeAddress(Request $request) {
        $allRequest = $request->all();
        if (Address::store($allRequest)) {
            return redirect()->back()->with('message', 'Thêm địa chỉ thành công');
        } else {
            return redirect()->back()->with('error', 'Thêm địa chỉ thất bại');
        }
    }

    public function updateAddress(Request $request) {
        $allRequest = $request->all();
        if (Address::updateAddress($allRequest)) {
            return redirect()->back()->with('message', 'Thêm địa chỉ thành công');
        } else {
            return redirect()->back()->with('error', 'Sửa địa chỉ thất bại');
        }
    }

    public function showProfilePage()
    {
        $loginUser = Auth::user();
        return view('pages.profile',
            [
                'loginUser' => $loginUser,
            ]);
    }

    public function updateUser(Request $request) {
        $allRequest = $request->all();
        $this->removeImage('public/'.Auth::user()->avatarUrl);
        if($request->file('avatar')) {
            $path = $request->file('avatar')->store('avatars', 'public');
        } else $path = null;
        if (User::updateUser($allRequest, $path)) {
            return redirect()->back()->with('message', 'Sửa thông tin thành công');
        } else {
            return redirect()->back()->with('error', 'Sửa thông tin thất bại');
        }
    }

    public function deleteAddress(Request $request,$id) {
        if (Address::deleteAddress($id)) {
            return redirect()->back()->with('message', 'Xóa sản phẩm thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa địa chỉ không thành công');
        }
    }

    public function showProductPage() {
        $products = Product::getAllProductbyUserId(Auth::user()->id);
        return view('pages.product-list',[
            'products'=>$products
        ]);
    }

    public function showProductForm(Request $request) {
        $productInfo = Product::getProductbyId($request->id) ?? null ;
        $categories = Category::getCategories();
        return view('pages.product-form',[
            'productInfo' => $productInfo,
            'categories' => $categories,
        ]);
    }

    public function storeProduct(Request $request) {
        $allRequest = $request->all();
        if($request->file('avatar')) {
            $path = $request->file('avatar')->store('products', 'public');
        } else $path = null;

        $lastId = Product::store($allRequest,$path);
        if($request->file('img-product-1')) {
            $path1 = $request->file('img-product-1')->store('products', 'public');
        }
        if($request->file('img-product-2')) {
            $path2 = $request->file('img-product-2')->store('products', 'public');
        }
        if($request->file('img-product-3')) {
            $path3 = $request->file('img-product-3')->store('products', 'public');
        }

        if (isset($path1)) {
            Image::store($lastId, null, $path1);
        }

        if (isset($path2)) {
            Image::store($lastId, null, $path2);
        }

        if (isset($path3)) {
            Image::store($lastId, null, $path3);
        }

        if (isset($lastId)) {
            return redirect('/profile/san-pham')->with('message', 'Đăng sản phẩm thành công');
        } else {
            return redirect()->with('error', 'Đăng sản phẩm không thành công');
        }
    }

    public function updateProduct(Request $request) {
        $allRequest = $request->all();
        if($request->file('avatar')) {
            $productInfo = Product::getProductbyId($request->id);
            if($productInfo->avatar) { $this->removeImage('public/'.$productInfo->avatar); }
            $path = $request->file('avatar')->store('products', 'public');
        } else $path = null;
        if (Product::updateProduct($allRequest, $path)) {
            return redirect('/profile/san-pham')->with('message', 'Sửa sản phẩm thành công');
        } else {
            return redirect()->with('error', 'Sửa sản phẩm không thành công');
        }
    }

    public function deleteProduct(Request $request, $id) {
        if (Product::deleteP($id)) {
            return redirect()->back()->with('message', 'Xóa sản phẩm thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa sản phẩm không thành công');
        }
    }

    public function changeSellingStatus(Request $request) {
        $productInfo = Product::getProductbyId($request->id);
        if($productInfo->isSold) {
            if (Product::changeSelling($request->id)) {
                return redirect()->back()->with('message', 'Đổi trạng thái sản phẩm thành công');
            };
        } else {
            if(Product::changeSold($request->id)) {
                return redirect()->back()->with('error', 'Đổi trạng thái sản phẩm không thành công');
            };
        }
        return dd(' Lỗi !');
    }

    public function storeImage(Request $request) {
        $path = $request->file('img-product')->store('products', 'public');

        if (isset($path)) {
            Image::store($request->product_id, null, $path);
            return redirect()->back()->with('message', 'Đăng hình ảnh thành công');
        } else {
            return redirect()->back()->with('message', 'Đăng hình ảnh không thành công');
        }
    }

    public function deleteImage(Request $request,$id) {
        $imageInfo = Image::findImageById($id);
        if (isset($imageInfo)) {
            $this->removeImage($imageInfo->url);
            Image::deleteImage($id);
            return redirect()->back()->with('message', 'Xóa hình ảnh thành công');
        } else {
            return redirect()->back()->with('message', 'Xóa hình ảnh không thành công');
        }
    }

    public function removeImage($path)
    {
        if( Storage::exists($path)){
             Storage::delete($path);
        }
//        else {
//            dd($path .' &nbsp File does not exists.');
//        }
    }

}

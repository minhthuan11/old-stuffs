<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function getRegister()
    {
        return view('auth/register');
    }

    public function postRegister(Request $request)
    {
        $allRequest = $request->all();
        $validator = $this->validator($allRequest);
        if ($validator->fails()) {
            return redirect('register')->withErrors($validator)->withInput();
        } else {
            if (User::create($allRequest)) {
                return redirect('login')->with('message', 'Đăng ký thành viên thành công!');
            } else {
                return redirect('register')->withErrors('Đăng ký thành viên thất bại');
            }
        }
    }

    protected function validator(array $data)
    {
        return Validator::make($data,
            [
                'full_name' => 'required|string|max:255',
                'username' => 'unique:users',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ],
            [
                'name.required' => 'Họ và tên là trường bắt buộc',
                'name.max' => 'Họ và tên không quá 255 ký tự',
                'email.required' => 'Email là trường bắt buộc',
                'email.email' => 'Email không đúng định dạng',
                'email.max' => 'Email không quá 255 ký tự',
                'email.unique' => 'Email đã tồn tại',
                'username.unique' => 'Username đã tồn tại',
                'password.required' => 'Mật khẩu là trường bắt buộc',
                'password.min' => 'Mật khẩu phải chứa ít nhất 8 ký tự',
                'password.confirmed' => 'Xác nhận mật khẩu không đúng',
            ]
        );
    }
}

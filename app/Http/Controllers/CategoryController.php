<?php

namespace App\Http\Controllers;

use App\Address;
use App\Category;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;

class CategoryController extends Controller
{
    function showIndexPage(Request $request, $id = null)
    {
        $provinces = Address::getProvinceVN();
        $categories = Category::getCategories();
        $promoted = Product::getAllPromotedByCategoryId($id);
        if (count($promoted) < 4) {
            $promoted = Product::getAllPromoted(4);
        }
        $query = Product::query();

        if ($id) {
            $query->where('category_id', '=', $id);
        }

        if ($request->category) {
            $query->where('category_id', '=', $request->category);
        }

        if ($request->province) {
            $addresses = Address::findAddressesbyProvince($request->province);
            foreach ($addresses as $address) {
                $array[] = $address->user_id;
            }
            $query->whereIn('user_id', $array);
//            $results = $query->paginate();
//            dd($results);
        }

        if ($request->minPrice) {
            $query->where('price', '>=', $request->minPrice);
        }

        if ($request->maxPrice) {
            $query->where('price', '<=', $request->maxPrice);
        }

        if ($request->promoted) {
            $query->where('isPromoted', '=', true);
        }

        if ($request->keyword) {
            $foundCategories = DB::table('categories')
                ->where('name', 'like', '%' . $request->keyword . '%')
                ->first();
            if ($foundCategories) {
                $query->where('title', 'like', '%' . $request->keyword . '%')
                    ->orWhere('description', 'like', '%' . $request->keyword . '%')
                    ->orWhere('category_id', '=', $foundCategories->id);
            } else {
                $query->where('title', 'like', '%' . $request->keyword . '%')
                    ->orWhere('description', 'like', '%' . $request->keyword . '%');
            }

            $promoted = Product::getAllPromotedByKeyWord($request->keyword);
            if (count($promoted) < 4) {
                $promoted = Product::getAllPromoted(4);
            }
        }

        $results = $query->paginate();
//        dd($results);
        $loginUser = Auth::user();
        return view('pages.category',
            [
                'promoted' => $promoted,
                'keyword' => $request->keyword,
                'results' => $results,
                'provinces' => $provinces,
                'categories' => $categories,
                'loginUser' => $loginUser,
            ]);
    }

}

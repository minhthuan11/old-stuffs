<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

//    public function __construct() {
//        $this->middleware('auth');
//    }
    function showIndexPage() {
        $products = Product::getAllProduct();
        $promotedProducts = Product::getAllPromoted();
        $premiumProducts = Product::getAllPromoted(3);
        $categories = Category::getAllCategory();
        $loginUser = Auth::user();
        return view('pages.index',[
            'products' => $products,
            'promotedProducts' => $promotedProducts,
            'premiumProducts' => $premiumProducts,
            'categories' => $categories,
            'loginUser' => $loginUser,
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Address;
use App\Category;
use App\Comment;
use App\Image;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    function showIndexPage(Request $request, $id) {
        $product = Product::getProductbyId($id);
        $category = Category::getCategorybyId($product->category_id);
        $user = User::getUserbyId($product->user_id);
        $address = Address::findAddressbyUserId($product->user_id);
        $loginUser = Auth::user();
        $comments = Comment::getCommentsbyProductId($product->id);
        $images = Image::getImagesbyProductId($product->id);
        return view('pages.product',
            [
                'product' => $product,
                'category' => $category,
                'user' => $user,
                'address' => $address,
                'loginUser' => $loginUser,
                'comments' => $comments,
                'images' => $images,
            ]
        );
    }

    public function storeCmt(Request $request) {
        $rating = null; $userRating = null;
        $allRequest = $request->all();
        $product = Product::getProductbyId($allRequest['product_id']);
        $user = User::getUserbyId($product->user_id);
        if( $request->file('cmt-url') ) {
            $path = $request->file('cmt-url')->store('comments', 'public');
        } else $path = null;
        if ( isset($allRequest['buyerRating']) ) {
            if ($product->averageRating) {
                $rating = ($product->averageRating + $allRequest['buyerRating']) / 2;
            } else {
                $rating = (int)$allRequest['buyerRating'];
            }
            if ($user->averageRating) {
                $userRating = ($user->averageRating + $allRequest['buyerRating']) / 2;
            } else {
                $userRating = (int)$allRequest['buyerRating'];
            }
        }

        $lastId = Comment::store($allRequest);

        if(isset($lastId))
        {
            if(isset($allRequest['buyerRating'])) {
                DB::table('products')->where('id', $product->id)->update(['averageRating' => $rating]);
                DB::table('users')->where('id', $user->id)->update(['averageRating' => $userRating]);
            }
            if ($path) {
                Image::store($allRequest['product_id'], $lastId, $path);
            }
            return redirect()->back()->with('message', 'Đăng đánh giá thành công');
        }
        else
        {
            return redirect()->back()->with('error', 'Đăng đánh giá không thành công');
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Address;
use App\Category;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    function showIndexPage(Request $request, $id) {
        $user = User::getUserbyId($id);
        $address = Address::findAddressbyUserId($id);
        $sellingProducts = Product::getAllSellingProductbyUserId($id);
        $soldProducts = Product::getAllSoldProductbyUserId($id);
        $categories = Category::getAllCategory();
        $loginUser = Auth::user();
        return view('pages.user',
        [
            'user' => $user,
            'address' => $address,
            'sellingProducts' => $sellingProducts,
            'soldProducts' => $soldProducts,
            'loginUser' => $loginUser,
            'categories' => $categories
        ]);
    }

}

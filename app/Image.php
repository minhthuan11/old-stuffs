<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Image extends Model
{
    protected $fillable = [
        'product_id','comment_id', 'url',
    ];

    public static function findImageById($id) {
        return DB::table('images')
            ->find($id);
    }

    public static function getImagesbyCommentId($id) {
        return DB::table('images')
            ->where('comment_id', $id)
            ->get();
    }

    public static function getImagesbyProductId($id) {
        return DB::table('images')
            ->where('product_id', $id)
            ->get();
    }

    public static function getProductImages($id) {
        return DB::table('images')
            ->where('product_id', $id)
            ->where('comment_id', '=', null)
            ->get();
    }

    public static function store($productId, $cmtId, $path)
    {
        return DB::table('images')->insert([
            'product_id' => $productId,
            'comment_id' => $cmtId,
            'url' => $path,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }

    public static function deleteImage($id) {
        return DB::table('images')
            ->where('id','=',$id)
            ->delete();
    }
}

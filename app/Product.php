<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $fillable = [
        'category_id', 'user_id','title', 'price','avatarUrl','imgUrl','description','isPromoted'
    ];

    public static function getAllProduct($limit = null) {
        if ($limit) {
            return DB::table('products')
                ->orderBy('id','desc')
                ->where('isSold', false)
                ->paginate($limit);
        } else {
            return DB::table('products')
                ->orderBy('id','desc')
                ->where('isSold', false)
                ->paginate(12);
        }
    }

    public static function getAllPromoted($limit = null) {
        if ($limit) {
            return DB::table('products')
                ->where('isPromoted',true)
                ->where('isSold', false)
                ->orderBy('id', 'desc')
                ->limit($limit)
                ->get();
        } else {
            return DB::table('products')
                ->where('isPromoted',true)
                ->where('isSold', false)
                ->orderBy('id', 'desc')
                ->limit(12)
                ->get();
        }
    }

    public static function getAllPromotedByCategoryId($id, $limit = null) {
        if ($limit) {
            return DB::table('products')
                ->where('category_id', $id)
                ->where('isPromoted',true)
                ->where('isSold', false)
                ->orderBy('id', 'desc')
                ->limit($limit)
                ->get();
        } else {
            return DB::table('products')
                ->where('category_id', $id)
                ->where('isPromoted',true)
                ->where('isSold', false)
                ->orderBy('id', 'desc')
                ->limit(4)
                ->get();
        }
    }

    public static function getAllPromotedByKeyWord($keyword) {
        $foundCategories = DB::table('categories')
            ->where('name', 'like', '%'.$keyword.'%')
            ->first();
        if( $foundCategories ) {
            return DB::table('products')
                ->where('title', 'like', '%' . $keyword . '%')
                ->orWhere('description', 'like', '%' . $keyword . '%')
                ->orWhere('category_id', '=', $foundCategories->id)
                ->where('isPromoted', true)
                ->where('isSold', false)
                ->orderBy('id', 'desc')
                ->limit(4)
                ->get();
        } else {
            return DB::table('products')
                ->where('title', 'like', '%' . $keyword . '%')
                ->orWhere('description', 'like', '%' . $keyword . '%')
                ->where('isPromoted', true)
                ->where('isSold', false)
                ->orderBy('id', 'desc')
                ->limit(4)
                ->get();
        }
    }

    public static function getProductbyId($id) {
        return DB::table('products')->find($id);
    }

    public static function getAllProductbyUserId($userid) {
        return DB::table('products')
            ->orderBy('id', 'desc')
            ->where('user_id', $userid)
            ->get();
    }

    public static function getAllProductByCategoryId($categoryId ,$limit = null) {
        if ($limit) {
            return DB::table('products')
                ->where('category_id', $categoryId)
                ->where('isSold', false)
                ->orderBy('id', 'desc')
                ->paginate($limit);
        } else {
            return DB::table('products')
                ->where('category_id', $categoryId)
                ->where('isSold', false)
                ->orderBy('id','desc')
                ->paginate(12);
        }
    }

    public static function getAllSellingProductbyUserId($id) {
        return DB::table('products')
            ->where('user_id', $id)
            ->where('isSold', false)
            ->get();
    }

    public static function getAllSoldProductbyUserId($id) {
        return DB::table('products')
            ->where('user_id', $id)
            ->where('isSold', true)
            ->get();
    }

    public static function store($request, $path)
    {
        return DB::table('products')->insertGetId([
            'category_id' => $request['category_id'],
            'user_id' => Auth::user()->id,
            'price' => $request['price'],
            'title' => $request['title'],
            'description' => $request['description'],
            'avatar' => $path,
            'isSold' => false,
            'isPromoted' => false,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }

    public static function updateProduct($request, $path) {
        if($path) {
            return DB::table('products')
                ->where('id', $request['id'])
                ->update([
                'category_id' => $request['category_id'],
                'user_id' => Auth::user()->id,
                'price' => $request['price'],
                'title' => $request['title'],
                'description' => $request['description'],
                'avatar' => $path,
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        } else {
            return DB::table('products')
                ->where('id', $request['id'])
                ->update([
                'category_id' => $request['category_id'],
                'user_id' => Auth::user()->id,
                'price' => $request['price'],
                'title' => $request['title'],
                'description' => $request['description'],
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }

    public static function changeSold($id) {
        return DB::table('products')
            ->where('id', $id)
            ->update([
                'isSold' => true,
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
    }

    public static function changeSelling($id) {
        return DB::table('products')
            ->where('id', $id)
            ->update([
                'isSold' => false,
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
    }

    public static function deleteP($id) {
        DB::table('comments')->where('product_id','=', $id)->delete();
        return DB::table('products')->where('id','=',$id)->delete();
    }

}

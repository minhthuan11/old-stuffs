<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','full_name', 'email', 'password','phone','avatarUrl'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getAllUser() {
        return DB::table('users')->get();
    }

    public static function getUserbyId($id) {
        return DB::table('users')->find($id);
    }

    public static function create($request)
    {
        return DB::table('users')->insert([
            'full_name' => $request['full_name'],
            'username' => $request['username'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'password' => Hash::make($request['password']),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }

    public static function updateUser($request, $path) {
        if($path) {
            return DB::table('users')
                ->where('id', Auth::user()->id)
                ->update([
                    'full_name' => $request['full_name'],
                    'email' => $request['email'],
                    'avatarUrl' => $path,
                    'phone' => $request['phone'],
                    'updated_at' => date("Y-m-d H:i:s"),
                ]);
        } else {
            return DB::table('users')
                ->where('id', Auth::user()->id)
                ->update([
                    'full_name' => $request['full_name'],
                    'email' => $request['email'],
                    'phone' => $request['phone'],
                    'updated_at' => date("Y-m-d H:i:s"),
                ]);
        }
    }

}

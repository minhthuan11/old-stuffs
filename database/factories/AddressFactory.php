<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'user_id' => $faker->unique()->numberBetween($min = 1, $max = 99),
        'street' => $faker->secondaryAddress ,
        'ward' => $faker->streetName,
        'district' => $faker->city,
        'province' => $faker->state,
        'created_at' => new DateTime,
        'updated_at' => new DateTime
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'category_id' => $faker->numberBetween($min = 1, $max = 6),
        'user_id' => $faker->numberBetween($min = 1, $max = 99),
        'title' => $faker->text,
        'price' => $faker->numberBetween($min = 10000, $max = 10000000) ,
        'avatar' => null,
        'description' => $faker->realText($maxNbChars = 5000, $indexSize = 2),
        'averageRating' => $faker->randomFloat($nbMaxDecimals = 1, $min = 0, $max = 5),
        'isSold' => $faker->boolean,
        'isPromoted' => $faker->boolean,
        'created_at' => new DateTime,
        'updated_at' => new DateTime
    ];
});

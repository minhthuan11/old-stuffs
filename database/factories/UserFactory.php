<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'username' => $faker->unique()->userName,
        'full_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'avatarUrl' => 'https://www.w3schools.com/howto/img_avatar.png',
        'phone' => $faker->e164PhoneNumber,
        'averageRating' => $faker->randomFloat($nbMaxDecimals = 1, $min = 0, $max = 5),
        'password' => $faker->password,
        'created_at' => new DateTime,
        'updated_at' => new DateTime
    ];
});

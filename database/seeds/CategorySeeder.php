<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Laptop & Máy tính',
                'imgUrl'=>'https://i.pinimg.com/originals/ef/18/20/ef1820929d8dadef4e8d97bd6383e87a.jpg',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Điện thoại',
                'imgUrl'=>'https://i.pinimg.com/originals/da/52/37/da5237264b09af44caa95c8574d08a0a.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Tivi',
                'imgUrl'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQKVEk1AT3fsgi4iCT80ncOFRrxG3y9r-zg-h3bnVLGkyWS0ar_&usqp=CAU',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Xe máy',
                'imgUrl'=>'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/512x512/motorbike.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Phụ Kiện Thời Trang',
                'imgUrl'=>'https://cdn2.iconfinder.com/data/icons/woman-accessories-outline/64/necklace-512.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Ô tô',
                'imgUrl'=>'https://img.icons8.com/plasticine/2x/car.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Thời Trang Nam',
                'imgUrl'=>'https://cdn1.iconfinder.com/data/icons/e-commerce-50/64/x-10-512.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Thời Trang Nữ',
                'imgUrl'=>'https://cdn1.iconfinder.com/data/icons/women-s-clothing-2-flat/64/female_clothes-02-512.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Nhà Cửa & Đời Sống',
                'imgUrl'=>'https://w1.pngwave.com/png/617/443/706/hand-icon-family-life-icon-heart-icon-symbol-logo-house-jersey-png-clip-art.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Giày Dép Nam',
                'imgUrl'=>'https://cdn2.iconfinder.com/data/icons/men-s-accessories-flat-colorful/2048/7776_-_Formal_Shoes-512.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Giày Dép Nữ',
                'imgUrl'=>'https://cdn3.iconfinder.com/data/icons/attire-essentials-colored/48/JD-44-512.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Thiết Bị Điện Gia Dụng',
                'imgUrl'=>'https://w7.pngwing.com/pngs/383/352/png-transparent-black-front-load-washing-machine-illustration-washing-machine-laundry-cleaning-icon-blue-plane-simple-washing-machine-icon-electronics-simple-text.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Đồng Hồ',
                'imgUrl'=>'https://www.pngkey.com/png/detail/871-8710663_apple-watch-png-apple-watch-icon-png.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Đồ Chơi',
                'imgUrl'=>'https://img.freepik.com/free-vector/retro-80s-style_24911-48698.jpg?size=338&ext=jpg',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Mẹ và Bé',
                'imgUrl'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQjYsDxJrfE5qdQUiRDX5Defp5HBE-g1fq0pEgxyrpt6xe1W-Lb&usqp=CAU',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Sức Khỏe & Sắc Đẹp',
                'imgUrl'=>'https://www.nicepng.com/png/detail/133-1338024_health-icon-heart-feel-icon-png.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Thể Thao',
                'imgUrl'=>'https://image.freepik.com/free-vector/table-tennis-icon-racket-ball-ping-pong-sport-icon-isolated_138676-524.jpg',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Du Lịch ',
                'imgUrl'=>'https://w1.pngwave.com/png/150/975/717/world-icon-travel-icon-airport-icon-logo-emblem-symbol-png-clip-art.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Phụ kiện điện tử',
                'imgUrl'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTRvsrbY1oy_3xATWGa5h0GCVxn7lyxbsOmsQ73g9s8PTwEQV4Z&usqp=CAU',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
            'name' => 'Mỹ phẩm',
            'imgUrl'=>'https://cdn3.iconfinder.com/data/icons/beauty-ultra-color/61/041_-_Makeup-512.png',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Sách',
                'imgUrl'=>'https://www.nicepng.com/png/detail/10-101646_borrow-library-books-book-stack-books-icon.png',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
        ]);
    }
}

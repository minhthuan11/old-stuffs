require('./bootstrap');
require('../../node_modules/jquery/dist/jquery.js');
require('../../node_modules/owl.carousel/dist/owl.carousel.min.js');

//require('/node_modules/jquery/dist/jquery.js');
//require('/node_modules/owl.carousel/dist/owl.carousel.min.js');

$(document).ready(function(){
    $('.owl-carousel').owlCarousel();
    $('label').click(function() {
        $('label').removeClass('active');
        $(this).addClass('active');
    });
});

$('.custom-file-input').on('change',function(e){
    let fileName = e.target.files[0].name;
    $(this).next('.custom-file-label').html(fileName);
})

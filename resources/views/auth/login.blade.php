@extends('auth.header')
@section('content')
<?php //Hiển thị thông báo thành công?>
@if ( Session::has('success') )
    <div class="alert alert-success alert-dismissible" role="alert">
        <strong>{{ Session::get('success') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
@endif
<?php //Hiển thị thông báo lỗi?>
@if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <strong>{{ Session::get('error') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
        </button>
    </div>
@endif
<style>
    body {
        background-image: url("{{ asset('images/login.jpg') }}");
        background-repeat: no-repeat;
        background-size: cover;
    }
</style>
<body>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-4 m-auto">
            <div class="form-login-user">
                <form action="{{ url('/login') }}" method="POST" class="form-group" style="margin-bottom: 2px!important;">
                    {!! csrf_field() !!}
                    <label >Tên đăng nhập</label>
                    <div class="input-group ">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-user-circle"></i></div>
                        </div>
                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Tên đăng nhập" name="username">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mật khẩu</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-lock"></i></div>
                            </div>
                            <input type="password" class="form-control"  placeholder="Mật khẩu" name="password">
                        </div>
                        <div class="form-group form-check" style="margin-bottom: 5px!important;">
                            <input type="checkbox" class="form-check-input" style="width: 15px;height: 15px;" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1" >Ghi nhớ</label> <br>
                        </div>
                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="fas fa-sign-in-alt" style="margin-right: 5px;;"></i>Đăng Nhập</button>
                        <a href="/" >Quay về trang chủ</a>
                        <hr>
                        <a href="/register" class="btn btn-outline-danger btn-block"><i class="fas fa-user-plus" style="margin-right: 5px;;"></i>Đăng Ký</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

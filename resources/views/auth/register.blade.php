@extends('auth.header')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
    @endif
    <body>
        <div class="container" style="margin-top: 10%">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4 p-3" style="background: rgba(253, 236, 236, 0.85);; border-radius: 10px">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="color: black">ĐĂNG KÝ THÀNH VIÊN</h4>
                        </div>
                        <div class="panel-body">
                            <form role="form" method="POST" action="{{ url('/register') }}">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input class="form-control" placeholder="Họ và tên" name="full_name" type="text"
                                               value="{{ old('name') }}" autofocus>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input class="form-control" placeholder="Tài khoản" name="username" type="text"
                                               value="{{ old('name') }}" autofocus>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input class="form-control" placeholder="Số điện thoại" name="phone" type="tel"
                                               value="{{ old('name') }}" autofocus>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                        <input class="form-control" placeholder="Email" name="email" type="text"
                                               value="{{ old('email') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input class="form-control" placeholder="Password" name="password" type="password"
                                               value="{{ old('name') }}" autofocus>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input class="form-control" placeholder="Xác nhận mật khẩu"
                                               name="password_confirmation" type="password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-lg btn-primary btn-block">Đăng ký</button>
                                </div>
                                <center><a style="color: #4d9ffa" href="/">Quay về trang chủ</a></center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <style>
        body {
            background-image: url("{{ asset('images/register.jpg') }}");
            background-repeat: no-repeat;
            background-size: cover;
        }

        .panel {
            border-radius: 5px;
        }

        .panel-heading {
            padding: 10px 15px;
        }

        .panel-title {
            text-align: center;
            font-size: 15px;
            font-weight: bold;
            color: #17568C;
        }

        .panel-footer {
            padding: 1px 15px;
            color: #A0A0A0;
        }

        .profile-img {
            width: 120px;
            height: 120px;
            margin: 0 auto 10px;
            display: block;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
        }
    </style>
@endsection

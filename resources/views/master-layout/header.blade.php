<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>@yield('title')</title>
    <title>Document</title>
</head>
<body>
<div class="section-header" style="background: rgb(8, 160, 92)">
    <div class="section-header">
        <div class="container">
            <div class="row">
                <div class="my-2">
                    <a href="#" class="px-2 menu-item"> Tải Ứng Dụng </a>
                    <span class="px-2" style="color: white">Kết nối</span>
                    <a href="#" class="mx-2"><i class="fab fa-facebook icon-header-10"></i></a>
                    <a href="#" class="mx-2"><i class="fab fa-instagram icon-header-10"></i></a>

                </div>
                <div class="my-2 ml-auto">
                    <a href="#" class="mx-2 .menu-item"><i class="far fa-bell"></i> Thông báo </a>
                    <a href="#" class="mx-2 .menu-item"><i class="far fa-question-circle"></i> Trợ giúp </a>

                    <?php
                    use Illuminate\Support\Facades\Auth;if (Auth::user()) {

                        echo '<a href="/tai-khoan/{{ \Illuminate\Support\Facades\Auth::user()->id }}" class="px-2 menu-item"> Trang cá nhân </a>
                            <span class="text-white">Xin chào,</span><a href="/profile" class="mx-2 .menu-item">'. Auth::user()->full_name.' </a>

                            <a href="/logout"> Đăng xuất </a>';
                    } else {
                        echo '
                            <a href="/register" class="mx-2 .menu-item"> Đăng Ký</a>
                            <span class="border-right-10"></span>
                            <a href="/login" class="mx-2 .menu-item"> Đăng Nhập </a>
                        '; //end of echo
                    }
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    <a href="/" class="mx-auto">
                        <img class="w-100"
                             src="{{ asset('images/logo.png') }}"
                             alt="">
                    </a>

                </div>

                <div class="col-10">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="/danh-muc" method="get">
                                <input style="width: 100%;padding: 1.375rem .75rem;" type="text"
                                       placeholder="Search something ..."
                                       class="form-control d-inline-block searching-button"
                                       name="keyword" id="">
                                <button type="submit" class="btn btn-primary searching-button-10 ">Search </button>                               </button>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 my-2" style="font-size: .75rem;">
                            <?php
                            $headerCategories = \App\Category::getAllCategory(10);
//                            var_dump($headerCategories);
                            foreach ( $headerCategories as $category ) {
                                echo '<a href="/danh-muc/'. $category->id .'" class=" mx-2 .menu-item">'. $category->name .'</a>';
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@if (\Session::has('message'))
    <div class="alert alert-success alert-dismissible " role="alert">
        <div style=" z-index: 1">
            <div>{!! Session::get('message') !!}</div>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if (Session::has('error'))
    <div class="alert alert-danger alert-dismissible" role="alert">
        <div style="position: absolute; top: 50%; left: 0; z-index: 1">
            <div>{!! Session::get('error') !!}</div>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@section('content')
@show

<hr style="margin: 10px 0; color: grey">

<footer>
    <div class="container-fluid bg-white">
        <div class="container" style="padding: 15px;">
            <div class="row" style="color: black!important;">
                <div class="col">
                    <h4>Liên Kết</h4>
                    <i class="fab fa-facebook-square" style="font-size: 20px;margin-right: 2px;"></i>
                    <a href="#" class="text-a-user " style="font-size: 20px;">Facebook</a>
                    <br>
                    <i class="fab fa-google-plus-square" style="font-size: 20px;margin-right: 2px;"></i><a href="#" style="font-size: 20px;" class="text-a-user ">Google</a>
                    <br>
                    <i class="fab fa-youtube" style="font-size: 18px;margin-right: 2px;"></i><a href="#" style="font-size: 20px;" class="text-a-user ">Youtube</a>
                </div>
                <div class="col">
                    <h4>Bản di động</h4>
                    <a href="#"><img alt="App Store" height="32"
                                     src="https://static.chotot.com/storage/marketplace/ios-logo.svg"></a> <br>
                    <a href="#"><img alt="Google Play" height="32"
                                     src="https://static.chotot.com/storage/marketplace/gg-play.svg"></a>
                </div>
                <div class="col">
                    <h4>Hỗ Trợ Khách Hàng</h4>
                    <a href="#" class="text-a-user ">Trung Tâm Trợ Giúp</a> <br>
                    <a href="#" class="text-a-user ">An Toàn Mua Bán</a> <br>
                    <a href="#" class="text-a-user ">Quy Định Cần Biết</a> <br>
                    <a href="#" class="text-a-user ">Liên Hệ Hỗ Trợ</a>

                </div>
                <div class="col">
                    <h4> Về Oldstuff.com </h4>
                    <a href="#" class="text-a-user ">Giới Thiệu</a> <br>
                    <a href="#" class="text-a-user ">Tuyển Dụng</a> <br>
                    <a href="#" class="text-a-user ">Truyền Thông</a> <br>
                    <a href="#" class="text-a-user ">Liên Hệ</a>
                </div>
                <div class="col">
                    <h4>Chứng Nhận</h4>
                    <img alt="Certification" style="height:38px;width:123px"
                         src="https://static.chotot.com/storage/marketplace/cerfiticate.png">
                </div>
            </div>
            <address style="text-align: center;color: #555;font-size: 14px;margin-top: 10px;">
                CÔNG TY TNHH OLD STUFF - Địa chỉ: Phòng 1808, Tầng 18, Mê Linh Point Tower, 02 Ngô Đức Kế, Phường Bến
                Nghé, Quận 1, TP Hồ Chí Minh
                Giấy chứng nhận đăng ký doanh nghiệp số 0312120782 do Sở Kế Hoạch và Đầu Tư TPHCM cấp ngày
                11/01/2013
                Email: trogiup@oldstuff.vn - Đường dây nóng: (028)38664041
            </address>
        </div>
    </div>
</footer>

</body>
<script src="https://kit.fontawesome.com/2ee40ea942.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.0.min.js"
    integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}"></script>
</html>

@extends('master-layout.header')

@section('content')
    @parent

    <div class="container my-5">
        <div class="row">
            <div class="col-md-3">
                <a href="/profile/sua-thong-tin/">
                    <div class="nav-profile "><span class=""> <i class="fa fa-user-circle-o mx-2" aria-hidden="true"></i>Sửa thông tin </span></div>
                </a>
                <a href="/profile/san-pham">
                    <div class="nav-profile"><span class=""> <i class="fa fa-server mx-2" aria-hidden="true"></i>Quản lý sản phẩm </span></div>
                </a>
                <a href="/profile/dia-chi">
                    <div class="nav-profile nav-profile-active"><span class=""> <i class="fa fa-address-book-o mx-2" aria-hidden="true"></i>Quản lý địa chỉ </span></div>
                </a>
            </div>
            <div class="col-md-8">
                @if(!$addressInfo)
                    <form action="/profile/dia-chi/create" method="post">
                        @csrf
                        {{--                    <div class="row">--}}
                        {{--                        <div class="col-3"><label for="disabledTextInput">Họ tên</label></div>--}}
                        {{--                        <div class="col-9"><input type="text" id="disabledTextInput" class="form-control"></div>--}}
                        {{--                    </div>--}}
                        <div class="row mt-2">
                            <div class="col-3">
                                <label for="disabledTextInput">Tỉnh/Thành Phố</label>
                            </div>
                            <div class="col-9">
                                <select class="form-control province" name="province" required>
                                    <?php
                                    foreach ($provinces as $province) {
                                        echo '<option value="' . $province->_name . '" >' . $province->_name . ' </option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-3">
                                <label for="disabledTextInput">Quận huyện</label>
                            </div>
                            <div class="col-9">
                                <select class="form-control" name="district">
                                    <option value="District 1"> Quận 1</option>
                                    <option value="District 2"> Quận 2</option>
                                    <option value="District 3"> Quận 3</option>
                                    <option value="District 4"> Quận 4</option>
                                    <option value="District 5"> Quận 5</option>
                                    <option value="District 6"> Quận 6</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-3">
                                <label for="disabledTextInput">Phường Xã</label>
                            </div>
                            <div class="col-9">
                                    <select class="form-control" name="ward">
                                        <option value="Ward 1"> Phường 1</option>
                                        <option value="Ward 2"> Phường 2</option>
                                        <option value="Ward 3"> Phường 3</option>
                                        <option value="Ward 4"> Phường 4</option>
                                        <option value="Ward 5"> Phường 5</option>
                                        <option value="Ward 6"> Phường 6</option>
                                    </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-3">
                                <label for="disabledTextInput" style="margin-top: 20px;">Địa chỉ</label>
                            </div>
                            <div class="col-9">
                                <div class="form-group">
                                    <textarea class="form-control" name="street" rows="3"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-3"></div>
                            <div class="col-9">
                                <button type="submit" class="btn btn-warning">Thêm</button>
                            </div>
                        </div>
                    </form>
                @else

                    <form action="/profile/dia-chi/update" method="post">
                        @csrf
                        <div class="row">
                            <input type="hidden" value="{{ $addressInfo->id }}" name="id" class="form-control" readonly>
                        </div>
                        <div class="row mt-2">
                            <div class="col-3">
                                <label for="disabledTextInput">Tỉnh/Thành Phố</label>
                            </div>
                            <div class="col-9">
                                <select class="form-control" name="province">
                                    <?php
                                    foreach ($provinces as $province) {
                                        if ( $addressInfo->province === $province->_name) {
                                            echo '<option value="' . $province->_name . '" selected>' . $province->_name . ' </option>';
                                        } else {
                                            echo '<option value="' . $province->_name . '" >' . $province->_name . ' </option>';
                                        }

                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-3">
                                <label for="disabledTextInput">Quận huyện</label>
                            </div>
                            <div class="col-9">
                                <select class="form-control" name="district">
                                    <option value="District 1"> Quận 1</option>
                                    <option value="District 2"> Quận 2</option>
                                    <option value="District 3"> Quận 3</option>
                                    <option value="District 4"> Quận 4</option>
                                    <option value="District 5"> Quận 5</option>
                                    <option value="District 6"> Quận 6</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-3">
                                <label for="disabledTextInput">Phường Xã</label>
                            </div>
                            <div class="col-9">
                                <select class="form-control" name="ward">
                                    <option value="Ward 1"> Phường 1</option>
                                    <option value="Ward 2"> Phường 2</option>
                                    <option value="Ward 3"> Phường 3</option>
                                    <option value="Ward 4"> Phường 4</option>
                                    <option value="Ward 5"> Phường 5</option>
                                    <option value="Ward 6"> Phường 6</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-3">
                                <label for="disabledTextInput" style="margin-top: 20px;">Địa chỉ</label>
                            </div>
                            <div class="col-9">
                                <div class="form-group">
                                    <textarea  class="form-control" name="street" rows="3">{{ $addressInfo->street }}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-3"></div>
                            <div class="col-9">
                                <button type="submit" class="btn btn-warning">Sửa</button>
                            </div>
                        </div>
                    </form>

                @endif

            </div>
        </div>
    </div>

@endsection

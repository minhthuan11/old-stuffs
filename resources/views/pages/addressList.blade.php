@extends('master-layout.header')

@section('content')
    @parent

    <div class="container my-5 ">
        <div class="row">
            <div class="col-md-3">
                <a href="/profile/sua-thong-tin/">
                    <div class="nav-profile "><span class=""> <i class="fa fa-user-circle-o mx-2" aria-hidden="true"></i>Sửa thông tin </span></div>
                </a>
                <a href="/profile/san-pham">
                    <div class="nav-profile"><span class=""> <i class="fa fa-server mx-2" aria-hidden="true"></i>Quản lý sản phẩm </span></div>
                </a>
                <a href="/profile/dia-chi">
                    <div class="nav-profile nav-profile-active"><span class=""> <i class="fa fa-address-book-o mx-2" aria-hidden="true"></i>Quản lý địa chỉ </span></div>
                </a>
            </div>
            <div class="col-md-8">
                <div class="new-add-addresses">
                    <a href="/profile/dia-chi/create">
                        <div  style="text-align: center" class="nav-profile-active p-3">
                            <i class="fas fa-plus" style="color: rgb(120, 120, 120);"></i> <span>Thêm địa chỉ mới</span>
                        </div>
                    </a>
                </div>
                @foreach($addresses as $address)
                    <div class="show-addresses-circle-check mt-2">
                        <div>
                            <div style="margin-bottom: 10px;">
                                <span>{{ $loginUser->full_name }}</span> <span style="font-size: 13px;margin-left: 5px;color: #26bc4e;"> </span>
                            </div>
                            <div style="font-size: 13px;">
                                <span style="color: rgb(120, 120, 120);">Địa chỉ :</span> <span> {{ $address->street }}, {{ $address->ward }}, {{ $address->district }}, {{ $address->province }} </span> <br>
                                <span style="color: rgb(120, 120, 120);">Điện thoại :</span> <span>{{ $loginUser->phone }}</span> <br>
                            </div>
                        </div>
                        <div  style="position: absolute;right: 20px;top: 10px;">
                            <a style="color: rgb(72,157,99)" class="nut-xoa" href="/profile/dia-chi/create/{{ $address->id }}" > Chỉnh sửa</a>
                            <a href="/profile/dia-chi/{{ $address->id }}/destroy" class="nut-xoa"> Xóa</a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>


@endsection

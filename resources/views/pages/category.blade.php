@extends('master-layout.header')

@section('content')
    @parent

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-3" style="border: 1px solid rgba(177,177,177,0.3); border-radius: 10px; height: 400px">
                <form action="/danh-muc" method="get">
{{--                    @csrf--}}
                    <div class="category-thuonghieu">
                        <div class="thuonghieu">
                            Danh mục
                        </div>
                        <div class="sanpham-thuonghieu">
                            <div class="form-group">
                                <select class="form-control" name="category">
                                    <option value="">Select an option ...</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }} ">{{ $category->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="category-thuonghieu">
                        <div class="thuonghieu">
                            Địa Điểm
                        </div>
                        <div class="sanpham-thuonghieu">
                            <div class="form-group">
                                <div class="name-product-checkbox">
                                    <select class="form-control" name="province">
                                        <option value="">Select an option ...</option>
                                        @foreach($provinces as $province)
                                            <option value="{{ $province->_name }} ">{{ $province->_name }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="thuonghieu">Giá</div>
                        <div class=" h-100">
                            <div class="gia-product-1">
                                <input type="number" min="0" class="c30Om7" name="minPrice" placeholder="Tối thiểu" value=""
                                       pattern="[0-9]*">
                                -
                                <input type="number" min="0" class="c30Om7" name="maxPrice" placeholder="Tối đa" value="" pattern="[0-9]*">
                            </div>
                        </div>
                    </div>
                    <div class="name-product-checkbox my-3">
                        <input type="checkbox" name="promoted">
                        <label for="brand-1">Được tài trợ</label>
                    </div>
                    <button type="submit" class="btn btn-outline-success  btn-block "
                            style="width:98%;margin-top: 20px;">ÁP DỤNG
                    </button>
                </form>
            </div>

            <div class="col-md-9">
                <div class="box-sap-xep">
                    <span class="sap-xep">Sắp xếp theo</span>
                    <div class="sort-by-option">
                        <div class="shopee-sort-by-options__option shopee-sort-by-options__option--selected">Liên quan
                        </div>
                        <div class="shopee-sort-by-options__option">Mới nhất</div>
                        <div class="shopee-sort-by-options__option">Bán chạy</div>
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>Sắp xếp</option>
                            <option>Thấp tới Cao</option>
                            <option>Cao tới thấp</option>
                            <!-- <option>xem nhiều nhất</option> -->
                        </select>
                    </div>
                </div>
                <div class="box-sap-xep-tag-product-10">
                    <div class="box-sap-xep-tag-10" style="text-align: center">
                        <h4 class="box-uu-tien-10">Sản phẩm tài trợ</h4>
                    </div>
                </div>
                <div class="row">

                    <?php
                    use App\User;foreach ($promoted as $product) {
                        if (!$product->avatar) $product->avatar = 'products/default.png';
                        echo '
                        <div class="col-md-3">
                            <div class="box-new-product my-3">
                                <div class="tag-a" style="display: block; cursor: text">
                                    <a href="/san-pham/' . $product->id . '">
                                        <img class="box-img-new-product p-3"
                                             src="/storage/' . $product->avatar . '"
                                             alt="">
                                        <div class="box-text-new-product">
                                            <div class="box-text-name">
                                                <div class="product-title">
                                                    [' . \App\Address::findAddressbyUserId($product->user_id)->province . '] ' . $product->title . '
                                                </div>
                                            </div>
                                            <div class="name-seller-newsp">
                                                <a href="/tai-khoan/' . $product->user_id . '" class=""> ' . User::getUserbyId($product->user_id)->full_name . ' </a>
                                            </div>
                                            <div class="box-duoc-tai-tro" style="background: rgb(61,61,61);">
                                                <span>Tài trợ</span>
                                            </div>
                                            <div class="box-icon-product-10">
                                                <div class="rating-stars">
                                                    <div class="rating-stars__stars">';
                                                $star = round(User::getUserbyId($product->user_id)->averageRating);
                                                for ($i = 1; $i <= $star; $i++) {
                                                    echo '<span class="fa fa-star checked"></span>';
                                                }
                                                for ($i = 5; $i > $star; $i--) {
                                                    echo '<span class="fa fa-star"></span>';
                                                }
                                                echo '
                                                    </div>
                                                </div>
                                                <div class="product-box-footer ">Đã bán ' . count(\App\Product::getAllSoldProductbyUserId($product->user_id)) . '</div>
                                            </div>
                                            <div class="box-text-price-product-10">
                                                <div class="fixed-price d-inline-block ml-auto">' . number_format($product->price, 0, '.', ',') . '<span class="price-money">₫</span></div>
                                            </div>

                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                        '; // End of echo
                    }
                    ?>
                        <div class="box-sap-xep-tag-product-10">
                            <div class="m-3" style="font-size: small; font-style: italic">
                                @if($keyword)
                                    Kết quả tìm kiếm:
                                @endif

                            </div>
                        </div>
                    <?php
                    foreach ($results as $product) {
                        if (!$product->avatar) $product->avatar = 'products/default.png';
                        echo '
                        <div class="col-md-3">
                            <div class="box-new-product my-3">
                                <div class="tag-a" style="display: block; cursor: text">
                                    <a href="/san-pham/' . $product->id . '">
                                        <img class="box-img-new-product p-3"
                                             src="/storage/' . $product->avatar . '"
                                             alt="">
                                        <div class="box-text-new-product">
                                            <div class="box-text-name">
                                                <div class="product-title">
                                                    [' . \App\Address::findAddressbyUserId($product->user_id)->province . '] ' . $product->title . '
                                                </div>
                                            </div>
                                            <div class="name-seller-newsp">
                                                <a href="/tai-khoan/' . $product->user_id . '" class=""> ' . User::getUserbyId($product->user_id)->full_name . ' </a>
                                            </div>
                                            <div class="box-icon-product-10">
                                                <div class="rating-stars">
                                                    <div class="rating-stars__stars">';
                                $star = round(User::getUserbyId($product->user_id)->averageRating);
                                for ($i = 1; $i <= $star; $i++) {
                                    echo '<span class="fa fa-star checked"></span>';
                                }
                                for ($i = 5; $i > $star; $i--) {
                                    echo '<span class="fa fa-star"></span>';
                                }
                                echo '
                                                    </div>
                                                </div>
                                                <div class="product-box-footer ">Đã bán ' . count(\App\Product::getAllSoldProductbyUserId($product->user_id)) . '</div>
                                            </div>
                                            <div class="box-text-price-product-10">
                                                <div class="fixed-price d-inline-block ml-auto">' . number_format($product->price, 0, '.', ',') . '<span class="price-money">₫</span></div>
                                            </div>

                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                        '; // End of echo
                    }
                    ?>
                        <div class="box-sap-xep-tag-product-10">
                            <div class="mx-auto" style="inline-size: max-content">
                                {{ $results->links() }}
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>

@endsection

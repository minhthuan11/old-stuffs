@extends('master-layout.header')

@section('title', 'Trang chủ')

@section('content')
    @parent


    <div class="container mt-5">
        <div class="row">
            <div class="col-md-7 ">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100 full-banner-home"
                                 src="https://cf.shopee.vn/file/5e735c5c788dfef8a2f919567172c1c8_xxhdpi"
                                 alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100 full-banner-home"
                                 src="https://cf.shopee.vn/file/af6885cf43dd13f89aa9e8c3f17d602c_xxhdpi"
                                 alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100 full-banner-home"
                                 src="https://cf.shopee.vn/file/f2e2dbeef25a83231b111d1ae1d236ac_xxhdpi"
                                 alt="Third slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

            <div class="col-md-5">
                <div class="row ">
                    <div class="col-md-12 ">
                        <a href="#"><img class="product2"
                                         src="https://cf.shopee.vn/file/9774c0c3327f2a4c984e267777fe9395_xhdpi" alt=""></a>
                    </div>
                    <div class="col-md-12 mt-1">
                        <a href="#"> <img class="product2"
                                          src="https://cf.shopee.vn/file/b374756c6a4c2ca1e5e5a1f3c1db70b7_xhdpi" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="box mt-5">
                <div class="box-category-header">
                    <h4>Danh mục</h4>
                </div>
                <div class="col-12">

                    <?php
                    use App\User;foreach ($categories as $category) {
                        echo '
                            <div class="p-2 card-category ">
                                <a href="/danh-muc/' . $category->id . '">
                                    <div class="mx-auto" style="height: 76px; width: 76px">
                                        <img class="w-100 h-100 rounded-circle"
                                        src="' . $category->imgUrl . '"
                                        alt="">
                                    </div>
                                    <div class="w-75 mx-auto">
                                        ' . $category->name . '
                                    </div>
                                </a>
                            </div>
                        ';
                    }
                    ?>
                    <div class="box-sap-xep-tag-product-10 my-4" style="border: 0">
                        <div class="mx-auto" style="inline-size: max-content">
                            {{ $categories->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="box my-3" >
                <div class="box-category-header">
                    <h4>Sản phẩm hàng đầu</h4>
                </div>
            </div>
            @foreach( $premiumProducts as $product)
                <div class="col-4">
                    <div class="box-premium">
                        <div class="row mr-1 py-3" style="border: 1px solid rgba(207,207,207,0.5); border-radius: 10px; min-height: 210px; position: relative">
                            <div class="" style="background-color: rgba(255, 0, 0, 0.8);color: white; position: absolute;
    text-align: center;
    text-transform: uppercase;
    font-weight: bold;
    z-index: 1;
    height: 42px;
    line-height: 43px;
    top: 5px;
    left: 5px;
    font-size: 12px;
    border-radius: 3px;
    padding: 0 6px;">
                                HOT
                            </div>
                            <div class="col-7">
                                <a href="/san-pham/{{ $product->id }}" style="cursor: pointer">
                                    <img  class="w-100 px-1" src="/storage/{{ $product->avatar }}" alt="">
                                </a>
                            </div>
                            <div class="col-5">
                                <div class="col-12">
                                    <a href="/san-pham/{{ $product->id }}"><div class="product-title mb-3">{{ $product->title }}</div></a>
                                    <a href="/tai-khoan/{{ $product->user_id }}"><div class="premium-title mb-3">{{ \App\User::getUserbyId($product->user_id)->full_name }}</div></a>

                                </div>
                                <div class="" style="position: absolute; bottom: 0; right: 31%">
                                    <div class="style-infor1">
                                        <a href="/danh-muc?province={{ \App\Address::findAddressbyUserId($product->user_id)->province }}"> <div style="font-size: small; font-style: italic" class="mt-0">{{ \App\Address::findAddressbyUserId($product->user_id)->province }}</div></a>
                                        <span style="font-size: 8px" > <i class="fa fa-star checked"></i> </span>
                                        <span style="font-size: 8px" > <i class="fa fa-star checked"></i> </span>
                                        <span style="font-size: 8px" > <i class="fa fa-star checked"></i> </span>
                                        <span style="font-size: 8px" > <i class="fa fa-star checked"></i> </span>
                                        <span style="font-size: 8px" > <i class="fa fa-star checked"></i> </span>
                                    </div>
                                    <div class="fixed-price d-inline-block ml-auto">{{ number_format($product->price, 0, ".", ",") }}<span class="price-money">₫</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        @endforeach
    </div>
    </div>

    <div class="container">
        <div class="row ">
            <h3 class="mx-auto my-5" style="font-size:50px;font-family: 'Ranga', cursive;"> Được tài trợ </h3>
        </div>

        <div class="row">

            <?php
            foreach ($promotedProducts as $promoted) {
                if (!$promoted->avatar) $promoted->avatar = 'products/default.png';
                echo '
                <div class="col-md-2">
                    <div class="box-new-product my-3">
                        <div class="tag-a" style="display: block; cursor: text">
                            <a href="/san-pham/' . $promoted->id . '">
                                <img class="box-img-new-product p-3"
                                     src="/storage/' . $promoted->avatar . '"
                                     alt="">
                                <div class="box-text-new-product">
                                    <div class="box-text-name">
                                        <div class="product-title">
                                            [' . \App\Address::findAddressbyUserId($promoted->user_id)->province . '] ' . $promoted->title . '
                                        </div>
                                    </div>
                                    <div class="name-seller-newsp">
                                        <a href="/tai-khoan/' . $promoted->user_id . '" class="sdafsda"> ' . User::getUserbyId($promoted->user_id)->full_name . ' </a>
                                    </div>
                                    <div class="box-duoc-tai-tro" style="background: rgb(8, 160, 92);">
                                        <span>Tài trợ</span>
                                    </div>
                                    <div class="box-icon-product-10">
                                        <div class="rating-stars">
                                            <div class="rating-stars__stars">';
                $star = round(User::getUserbyId($promoted->user_id)->averageRating);
                for ($i = 1; $i <= $star; $i++) {
                    echo '<span class="fa fa-star checked"></span>';
                }
                for ($i = 5; $i > $star; $i--) {
                    echo '<span class="fa fa-star"></span>';
                }
                echo '
                                            </div>
                                        </div>
                                        <div class="product-box-footer ">Đã bán ' . count(\App\Product::getAllSoldProductbyUserId($promoted->user_id)) . '</div>
                                    </div>
                                    <div class="box-text-price-product-10">
                                        <div class="fixed-price d-inline-block ml-auto">' . number_format($promoted->price, 0, ".", ",") . '<span class="price-money">₫</span></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                '; //endOfEcho
            }
            ?>

        </div>
    </div>

    <div class="container">
        <div class="row ">
            <h3 class="mx-auto my-5" style="font-size:50px;font-family: 'Ranga', cursive;"> Mới đăng </h3>
        </div>

        <div class="row">

            <?php

            foreach ($products as $product) {
                if (!$product->avatar) $product->avatar = 'products/default.png';
                echo '
                <div class="col-md-2">
                    <div class="box-new-product my-3">
                        <div class="tag-a" style="display: block; cursor: text">
                            <a href="/san-pham/' . $product->id . '">
                                <img class="box-img-new-product p-3"
                                     src="/storage/' . $product->avatar . '"
                                     alt="">
                                <div class="box-text-new-product">
                                    <div class="box-text-name">
                                        <div class="product-title">
                                            [' . \App\Address::findAddressbyUserId($product->user_id)->province . '] ' . $product->title . '
                                        </div>
                                    </div>
                                    <div class="name-seller-newsp">
                                        <a href="/tai-khoan/' . $product->user_id . '" class=""> ' . User::getUserbyId($product->user_id)->full_name . ' </a>
                                    </div>
                                    <div class="box-duoc-tai-tro" style="background: red; width: 80px;">
                                        <span>Mới đăng</span>
                                    </div>
                                    <div class="box-icon-product-10">
                                        <div class="rating-stars">
                                            <div class="rating-stars__stars">';
                $star = round(User::getUserbyId($product->user_id)->averageRating);
                for ($i = 1; $i <= $star; $i++) {
                    echo '<span class="fa fa-star checked"></span>';
                }
                for ($i = 5; $i > $star; $i--) {
                    echo '<span class="fa fa-star"></span>';
                }
                echo '
                                            </div>
                                        </div>
                                        <div class="product-box-footer ">Đã bán ' . count(\App\Product::getAllSoldProductbyUserId($product->user_id)) . '</div>
                                    </div>
                                    <div class="box-text-price-product-10">
                                        <div class="fixed-price d-inline-block ml-auto">' . number_format($product->price, 0, '.', ',') . '<span class="price-money">₫</span></div>
                                    </div>

                                </div>
                            </a>
                        </div>
                    </div>

                </div>
                '; // End of echo
            }
            ?>
        </div>

    </div>

@endsection


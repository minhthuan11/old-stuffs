@extends('master-layout.header')

@section('content')
    @parent

    <div class="container my-5">
        <div class="row">
            <div class="col-md-3">
                <a href="/profile/sua-thong-tin/">
                    <div class="nav-profile "><span class=""> <i class="fa fa-user-circle-o mx-2" aria-hidden="true"></i>Sửa thông tin </span></div>
                </a>
                <a href="/profile/san-pham">
                    <div class="nav-profile nav-profile-active"><span class=""> <i class="fa fa-server mx-2" aria-hidden="true"></i>Quản lý sản phẩm </span></div>
                </a>
                <a href="/profile/dia-chi">
                    <div class="nav-profile"><span class=""> <i class="fa fa-address-book-o mx-2" aria-hidden="true"></i>Quản lý địa chỉ </span></div>
                </a>
            </div>
            <div class="col-md-9">
                <div class="" id="ProductForm">
                    <div class="">
                        <div class="">
                            <div class="">
                                <div class="col-md-12  m-auto ">

                                    @if(!$productInfo)
                                    <h4 class="text-center" id="exampleModalLabel">Đăng sản phẩm</h4>
                                    <form action="/profile/san-pham/create" enctype="multipart/form-data" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Lựa chọn mục đăng tin </label>
                                            <select class="form-control" name="category_id"
                                                    id="exampleFormControlSelect1">
                                                <?php
                                                foreach ($categories as $category) {
                                                    echo '<option value="' . $category->id . '">' . $category->name . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Tên Sản Phẩm</label>
                                            <input type="text" name="title" class="form-control"
                                                   placeholder="Tên sản phẩm">
                                        </div>
                                        <div class="form-row mb-2">
                                            <div class="col">
                                                <label>Giá Sản Phẩm</label>
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"><i
                                                                class="fas fa-dollar-sign"></i>
                                                        </div>
                                                    </div>
                                                    <input type="number" class="form-control"
                                                           name="price" id="inlineFormInputGroup"
                                                           placeholder="Giá sản phẩm">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlFile1">Hình Đai Diện</label>
                                            <div class="input-group mb-3">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"
                                                           id="inputGroupFile011"
                                                           aria-describedby="inputGroupFileAddon011" name="avatar">
                                                    <label class="custom-file-label" for="inputGroupFile011">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlFile1">Hình Chi Tiết Sản Phẩm <small style="font-style: italic"> ( có thể thêm ở trang quản lý sản phẩm nếu cần nhiều hơn ) </small></label>
                                            <div class="custom-file mb-3">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="inputGroupFileAddon01">Hình 1</span>
                                                    </div>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input"
                                                               id="inputGroupFile01"
                                                               aria-describedby="inputGroupFileAddon01" name="img-product-1">
                                                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                    </div>
                                                </div>

                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="inputGroupFileAddon02">Hình 2</span>
                                                    </div>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input"
                                                               id="inputGroupFile01"
                                                               aria-describedby="inputGroupFileAddon02" name="img-product-2">
                                                        <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
                                                    </div>
                                                </div>

                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="inputGroupFileAddon03">Hình 3</span>
                                                    </div>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input"
                                                               id="inputGroupFile03"
                                                               aria-describedby="inputGroupFileAddon01" name="img-product-3">
                                                        <label class="custom-file-label" for="inputGroupFile03">Choose file</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Mô tả </label>
                                            <textarea class="form-control" name="description"
                                                      id="exampleFormControlTextarea1"
                                                      rows="5" style="white-space: pre-line;"></textarea>
                                        </div>
                                        <button type="submit" style="float: right" class="btn btn-outline-primary">
                                            Lưu lại
                                        </button>
                                    </form>
                                    @else
                                        <h4 class="text-center" id="exampleModalLabel">Cập nhập sản phẩm</h4>
                                        <form action="/profile/san-pham/update" enctype="multipart/form-data" method="post">
                                            @csrf
                                            <input class="form-control" type="hidden" name="id" value="{{ $productInfo->id }}" readonly>
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Lựa chọn mục đăng tin </label>
                                                <select class="form-control" name="category_id"
                                                        id="exampleFormControlSelect1">
                                                    <?php
                                                    foreach ($categories as $category) {
                                                        if($productInfo->category_id === $category->id) {
                                                            echo '<option value="' . $category->id . '" selected>' . $category->name . '</option>';
                                                        } else {
                                                            echo '<option value="' . $category->id . '">' . $category->name . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Tên Sản Phẩm</label>
                                                <input type="text" name="title" class="form-control"
                                                       placeholder="Tên sản phẩm" value="{{ $productInfo->title }}">
                                            </div>
                                            <div class="form-row mb-2">
                                                <div class="col">
                                                    <label>Giá Sản Phẩm</label>
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text"><i
                                                                    class="fas fa-dollar-sign"></i>
                                                            </div>
                                                        </div>
                                                        <input type="number" class="form-control"
                                                               name="price" id="inlineFormInputGroup"
                                                               placeholder="Giá sản phẩm" value="{{ $productInfo->price }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlFile1">Hình Đai Diện</label>
                                                <div class="input-group mb-3">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input"
                                                               id="inputGroupFile011"
                                                               aria-describedby="inputGroupFileAddon011" name="avatar">
                                                        <label class="custom-file-label" for="inputGroupFile011">Choose file</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlTextarea1">Mô tả </label>
                                                <textarea class="form-control" name="description"
                                                          id="exampleFormControlTextarea1" style="white-space: pre-wrap;"
                                                          rows="5"> {{ $productInfo->description }} </textarea>
                                            </div>
                                            <button type="submit"style="float: right" class="btn btn-outline-primary">
                                                Lưu lại
                                            </button>
                                        </form>

                                        @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('master-layout.header')

@section('content')
    @parent

    <div class="container my-5">
        <div class="row">
            <div class="col-md-3">
                <a href="/profile/sua-thong-tin/">
                    <div class="nav-profile "><span class=""> <i class="fa fa-user-circle-o mx-2" aria-hidden="true"></i>Sửa thông tin </span></div>
                </a>
                <a href="/profile/san-pham">
                    <div class="nav-profile nav-profile-active"><span class=""> <i class="fa fa-server mx-2" aria-hidden="true"></i>Quản lý sản phẩm </span></div>
                </a>
                <a href="/profile/dia-chi">
                    <div class="nav-profile"><span class=""> <i class="fa fa-address-book-o mx-2" aria-hidden="true"></i>Quản lý địa chỉ </span></div>
                </a>
            </div>
            <div class="col-md-9">
                <div class="new-add-addresses">
                    <a href="/profile/san-pham/create">
                        <div  style="text-align: center" class="nav-profile-active p-3">
                            <i class="fas fa-plus" style="color: rgb(120, 120, 120);"></i> <span>Thêm sản phẩm mới</span>
                        </div>
                    </a>
                </div>
                <div class="">
                    <div style="padding: 15px;">
                        <b>Quản lý thông tin sản phẩm</b>
                        <div class="gach-duoi-tag-buy"></div>
                    </div>
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Title</th>
                            <th scope="col">Giá</th>
                            <th scope="col">Hình sản phẩm</th>
                            <th scope="col">Hình chi tiết</th>
                            <th scope="col">Mô tả</th>
                            <th scope="col">Category</th>
                            <th scope="col">Created at</th>
                            <th scope="col">Update at</th>
                            <th scope="col">Trạng thái</th>
                            <th scope="col">Tài trợ</th>
                            <th scope="col">Hành Động</th>
                        </tr>
                        </thead>


                        <tbody>

                        <?php
                        use App\Category;foreach ($products as $key => $product) {
                            echo '
                                    <tr>
                                        <td>
                                            <button type="button" class="btn btn-outline-dark" data-toggle="modal"
                                                    data-target="#title' . $key . '">
                                                Xem
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="title' . $key . '" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Mô tả</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            ' . $product->title . '
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>

                                        <td>' . number_format($product->price, 0, '.', ',') . '<span class="price-money">₫</span></td>

                                        <td><img class="img-zoom-product-admin"
                                                 src="/storage/' . $product->avatar . '"
                                                 style="max-width: 100px; height: 60px; " alt="">
                                        </td>

                                        <td>
                                            <button type="button" class="btn btn-outline-dark" data-toggle="modal"
                                                    data-target="#ImageModal'. $key .'">
                                                Xem
                                            </button>

                                            <!-- Modal form -->
                                            <div class="modal fade" id="ImageModal'. $key .'" tabindex="-1" role="dialog"
                                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Quản lý hình ảnh sản phẩm</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="/hinh-anh/create" enctype="multipart/form-data" method="post" id="imageCreateForm">';
                                                                ?>
                                                                @csrf
                                                                <?php
                                                                echo'
                                                                <div class="input-group mb-3">
                                                                    <div class="custom-file">
                                                                        <input type="hidden" name="product_id" value="'. $product->id .' ">
                                                                        <input type="file" class="custom-file-input" onchange="this.form.submit()"
                                                                               aria-describedby="inputGroupFileAddon01" name="img-product">
                                                                        <label class="custom-file-label" for="inputImageFile1">Thêm hình ảnh ...</label>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            <table class="table">
                                                                <thead class="thead-dark">
                                                                <tr>
                                                                    <th scope="col">STT</th>
                                                                    <th scope="col">Image</th>
                                                                    <th scope="col">Hành động</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>';
                                                                $images = \App\Image::getProductImages($product->id);
                                                                foreach ($images as $key1 => $image) {
                                                                    echo '
                                                                    <tr>
                                                                        <th scope="row">'. ($key1+1) .'</th>
                                                                        <td><img class="img-zoom-product-admin"
                                                                                 src="/storage/'. $image->url .'"
                                                                                 style="max-width: 100px;height: 60px;" alt=""></td>
                                                                        <td>
                                                                            <a href="/hinh-anh/'. $image->id .'/delete"> <i style="color: red" class="fas fa-trash-alt icon-the-i"></i></a>
                                                                        </td>
                                                                    </tr>';
                                                                }
                                                                echo '
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close
                                                            </button>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>

                                        <td>
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-outline-dark" data-toggle="modal"
                                                    data-target="#modal' . $key . '">
                                                Xem
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="modal' . $key . '" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Mô tả</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            ' . $product->description . '
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>

                                        <td>
                                            <p>' . Category::getCategorybyId($product->category_id)->name . ' </p>
                                        </td>

                                        <td style="width: 140px;">' . $product->created_at . '</td>

                                        <td style="width: 140px;">' . $product->updated_at . '</td>
                                        <td>';
                                        if ($product->isSold) echo '<span style="color: dimgrey;"> Đã bán </span>'; else echo '<span style="color: green;"> Đang bán </span>';
                                        echo '
                                        </td>
                                        <td>
                                            '; //echo 1;

                                        if ($product->isPromoted) echo '<i class="fas fa-check p-0 m-0" style="color: green;"> </i>'; else echo '<i class="fas fa-times p-0 m-0" style="color: red;"></i >';

                                    echo '
                                        </td>
                                        <td>
                                            <a href="/profile/san-pham/create/'. $product->id .'"><i class="fas fa-pen-square icon-the-i btn p-0 m-0" data-toggle="modal" data-target="#GeneralForm" style="color: rgb(8, 160, 92);"></i></a>';
                                            if(!$product->isSold) {
                                                echo '<a href="/profile/san-pham/'. $product->id  . '/status"><i class="fa fa-handshake-o btn p-0 m-0" style="color: rgb(255,200,0);"></i></a>';
                                                } else {
                                                echo '<a href="/profile/san-pham/'. $product->id  .'/status"><i class="fa fa-cloud-upload btn p-0 m-0" style="color: rgb(255,200,0);"></i></a>';
                                            }
                                            echo '<i class="fas fa-trash-alt icon-the-i btn p-0 m-0" data-toggle="modal" data-target="#exampleModal" style="color: red;" ></i>
                                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    Bạn chắc chắn muốn xóa sản phẩm này ?
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                                    <a href="/profile/san-pham/' . $product->id . '/destroy"> <button type="button" class="btn btn-danger">Xóa</button> </a>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </td>
                                    </tr>
                                    '; //echo 2
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

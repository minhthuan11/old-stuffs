@extends('master-layout.header')

@section('content')
    <div class="container my-3 rounded" style="background: #f9f9f9">
        <div class="row">
            <div class="col-md-7 ">
                <div class="box-slideshow">
                    <div id="product-image-slide" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @if ( count($images) === 0 || count($images) === 1 )
                                <li data-target="#product-image-slide" data-slide-to="0" class="active"></li>
                            @else
                                <li data-target="#product-image-slide" data-slide-to="0" class="active"></li>
                                @for( $i=1; $i<count($images); $i++)
                                    <li data-target="#product-image-slide" data-slide-to="{{ $i }}"></li>
                                @endfor
                            @endif
                        </ol>
                        <div class="carousel-inner box-slideshow">
                            @if( count($images) === 0 )
                                <div class="carousel-item active">
                                    <img class="box-slideshow"
                                         src="https://lh3.googleusercontent.com/proxy/YQsrJY6KjMACSrM1QbGmMzutzY_uSsoNalYsj3ydAGwgTZ1r6j2Yy0ZwBFucdHTRhnYOEyqqoFRpVENP-Kz5GIzmNMuIQkKhsBgmzzcRrUbsQesndxdusw"
                                          alt="...">
                                    <p class="text-carousel"> Thời gian đăng:</p>
                                </div>
                        </div>
                            @else
                                @foreach( $images as $key => $image )
                                    @if($key === 0)
                                        <div class="carousel-item active">
                                            <img class="box-slideshow"
                                                 src="/storage/{{ $image->url }}"
                                                 alt="...">
                                            <p class="text-carousel">Thời gian đăng: {{ date('d-m-Y',strtotime($image->created_at)) }}</p>
                                        </div>
                                    @else
                                        <div class="carousel-item">
                                            <img class="box-slideshow"
                                                 src="/storage/{{ $image->url }}"
                                                 alt="...">
                                            <p class="text-carousel">Thời gian đăng: {{ date('d-m-Y',strtotime($image->created_at)) }}</p>
                                        </div>
                                    @endif
                                @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#product-image-slide" role="button"
                                   data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#product-image-slide" role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            @endif
                    </div>
                </div>
                <div class="box-decription">
                    <div class="name-title-product-detail">
                        <h1 class="font-h1"><?=$product->title?></h1>
                        <div class="font-price my-3"><?= number_format($product->price, 0, '.', ',')?><span class="price-money">&nbsp; ₫</span></div>
                    </div>
                    <div class="col-xs-12">
                        <pre><?=$product->description?></pre>
                    </div>
                </div>
            </div>

            <div class="col-md-5 user-infor-box" id="myHeader">
                <div class="box-infor-product-detail">

                    <img class="img-infor-product-detail"
                         src="/storage/{{$user->avatarUrl}}"
                         alt="">

                    <div class="name-product-detail">
                        <div class="name-detail">
                            <a style="color: #33a837" href="/tai-khoan/{{$user->id}}">{{$user->full_name}}</a>
                        </div>
                    </div>
                    <div class="style-infor-product-detail">
                        <div class="style-infor1">
                            <div><img src="{{$category->imgUrl}}" height="16"></div>
                            <div class="mt-auto mb-auto">{{$category->name}}</div>
                        </div>
                        <div class="border-box-1"></div>
                        <div class="style-infor1">
                            <div class="mt-0">Đánh Giá Sản phẩm</div>
                            <img class="sc-fMiknA caWVjK" alt="rating-star" width="13px"
                                 src="https://static.chotot.com.vn/storage/marketplace/common/pf_rating_active_icon.svg">
                            <img class="sc-fMiknA caWVjK" alt="rating-star" width="13px"
                                 src="https://static.chotot.com.vn/storage/marketplace/common/pf_rating_active_icon.svg">
                            <img class="sc-fMiknA caWVjK" alt="rating-star" width="13px"
                                 src="https://static.chotot.com.vn/storage/marketplace/common/pf_rating_active_icon.svg">
                            <img class="sc-fMiknA caWVjK" alt="rating-star" width="13px"
                                 src="https://static.chotot.com.vn/storage/marketplace/common/pf_rating_active_icon.svg">
                            <img class="sc-fMiknA caWVjK" alt="rating-star" width="13px"
                                 src="https://static.chotot.com.vn/storage/marketplace/common/pf_rating_active_icon.svg">
                        </div>
                        <div class="border-box-1"></div>
                        <div class="style-infor1">
                            <div class="mt-0">Địa chỉ</div>
                            <b>{{$address->province}}</b>
                        </div>
                    </div>

                </div>
                <div class="number-phone-product-detail">
                    <span><img class="kKKDsS"
                               src="https://static.chotot.com.vn/storage/chotot-icons/svg/white-phone.svg">
                        {{ substr($user->phone,0,7) }}...
                    </span>
                    &nbsp;&nbsp;
                    <span>Bấm để hiện số</span>
                </div>
                <div class="pay-product-detail">
                    <div class="request-loan-icon"></div>
                    <span>Cần tiền? Hãy vay của Home Credit</span>
                    <div class="powered-title-text">10-70 triệu</div>
                </div>
                <div class="quang-cao">
                    <div class="col-xs-12 mt-3">
                        <div><img alt="safe tips" class="pull-left" width="100"
                                  src="https://st.chotot.com/storage/images/tips/2_mobile.png">
                            <div class="mb-0 SafeTips__TipText-iJoGDM gYDniF">KHÔNG NÊN chuyển tiền trước khi nhận
                                hàng.
                            </div>
                            <a class="pull-left"
                               href="https://trogiup.chotot.com/mua-hang-tai-chotot-vn/meo-mua-hang-tim-viec/meo-khi-mua-dien-thoai/?utm_source=chotot&amp;utm_medium=ad_view&amp;utm_campaign=safety_tip_adview"
                               target="_blank">Tìm hiểu thêm »</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- comment rating -->
    <div class="container mt-2 " id="content">
        <div class="row box-review-user-for-buyer">
            <div class="col-md-3">
                @if($user->averageRating)
                    <div class="diem-tb-rating">
                        <h3 style="font-size: 16px; font-weight: 700;">Đánh giá của người bán</h3>
                        <div class="averageRating">
                            <p> {{ $user->averageRating }} <i class="fa fa-star checked" style="font-size: 40px;"></i> </p>
                        </div>
                    </div>
                @else
                    <div class="diem-tb-rating">
                        <h3 style="font-size: 16px; font-weight: 700;">Đánh giá của người bán</h3>
                        <div class="averageRating w-100">
                            <p style="font-size: 2rem; text-align: center"> Chưa có đánh giá </p>
                        </div>
                    </div>
                @endif
            </div>

            <div class="col-md-9">
                <form action="/comment/create" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="text-center mb-2" style="width: 100%;">
                        <p class="mb-1">Vui lòng chọn đánh giá :</p>
                        <div class="rating">
                            <label><span class="fa fa-star"></span>
                                <input type="radio" name="buyerRating" value="5" />
                            </label>
                            <label><span class="fa fa-star"></span>
                                <input type="radio" name="buyerRating" value="4" />
                            </label>
                            <label><span class="fa fa-star"></span>
                                <input type="radio" name="buyerRating" value="3" />
                            </label>
                            <label><span class="fa fa-star"></span>
                                <input type="radio" name="buyerRating" value="2" />
                            </label>
                            <label><span class="fa fa-star"></span>
                                <input type="radio" name="buyerRating" value="1" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-12 px-2">
                        <div class="form-group">
                            <label for="title">Tiêu đề:</label>
                            <input class="form-control" type="text" name="title" id="title">
                        </div>
                        <div class="form-group">
                            <label for="detail">Đánh giá:</label>
                            <textarea
                                class="form-control"
                                rows="2"
                                id="detail"
                                name="content"
                            ></textarea>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupFileAddon01">Hình ảnh</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01"
                                       aria-describedby="inputGroupFileAddon01" name="cmt-url">
                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <input type="hidden" readonly value="{{ $product->id }}" name="product_id">
                        <div class="col text-center" >
                            <button type="submit" class="btn btn-success">Gửi đánh giá</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- list Review -->
    <div class="container">

        <?php
            foreach ($comments as $comment) {
                echo '
                <div class="row my-3">
                    <div class="list-review">
                        <div class="col-md-12">
                            <div class="name-day-time">
                                <a href="/tai-khoan/'. $comment->buyer_id . '"><span style="font-style:italic ;font-weight: bold; color: #007aff">'. \App\User::getUserbyId($comment->buyer_id)->full_name .'</span> </a>
                                <span style="font-style:italic; font-size: smaller">Ngày ' . date('d/m/Y', strtotime($comment->created_at)) .'</span>
                            </div>
                            <div class="review-content">
                                <p class="mb-2">
                                <span>';
                                    for ($i=0; $i < $comment->buyerRating; $i++) {
                                        echo '<i class="fa fa-star checked"></i>';
                                    }
                                echo'
                                </span>
                                    <b class="p-3" >'. $comment->title .'</b>
                                    <p> '. $comment->content .' </p>
                                </p>';
                                $commentImages = \App\Image::getImagesbyCommentId($comment->id);
                                foreach ($commentImages as $image) {
                                    echo '<img src="/storage/'. $image->url .'" style="max-width: 150px; max-height: 150px;" alt="" />';
                                };
                                echo '
                            </div>
                        </div>
                    </div>
                </div>
                ';}
            ?>

    </div>

@endsection

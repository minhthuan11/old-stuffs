@extends('master-layout.header')

@section('content')
    @parent

    <div class="container my-5">
        <div class="row">
            <div class="col-md-3">
                <a href="/profile/sua-thong-tin/">
                    <div class="nav-profile nav-profile-active"><span class=""> <i class="fa fa-user-circle-o mx-2" aria-hidden="true"></i>Sửa thông tin </span></div>
                </a>
                <a href="/profile/san-pham">
                    <div class="nav-profile"><span class=""> <i class="fa fa-server mx-2" aria-hidden="true"></i>Quản lý sản phẩm </span></div>
                </a>
                <a href="/profile/dia-chi">
                    <div class="nav-profile"><span class=""> <i class="fa fa-address-book-o mx-2" aria-hidden="true"></i>Quản lý địa chỉ </span></div>
                </a>
            </div>
            <div class="col-md-8">

                <div class="" role="document">
                    <div class="">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Sửa thông tin cá nhân</h5>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="/profile/update" enctype="multipart/form-data" method="post" class="modal-body">
                            @csrf
                            <div class="form-group row">
                                <div class="col">
                                    <label>Họ tên</label>
                                    <input type="text" name="full_name" value="{{$loginUser->full_name}}" class="form-control">
                                </div>
                                <div class="col">
                                    <label>Số điện thoại</label>
                                    <input type="number" name="phone" value="{{$loginUser->phone}}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col">
                                    <label>Avatar</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input"
                                                   id="inputGroupFile01"
                                                   aria-describedby="inputGroupFileAddon01" name="avatar">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <label>Email</label>
                                    <input type="email" name="email" value="{{$loginUser->email}}" class="form-control">
                                </div>
                            </div>


                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Lưu lại</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

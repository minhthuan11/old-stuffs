@extends('master-layout.header')
<?php use App\Category;$isEdit = false; ?>
@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="container" style="background-color: white;border-radius: 5px;width: 100%;height: 100%;">
        <div class="row" style="padding-top: 10px;padding-bottom: 10px;">
            <div class="col-md-6 " style="border-right: 1px solid #9b9b9b;">
                <div class="row">
                    <div class="col-3">
                        @if($user->avatarUrl)
                        <img src="/storage/{{ $user->avatarUrl }}"
                             class="infor-user-img-10" alt="">
                        @else
                            <img src="https://image.flaticon.com/icons/png/512/21/21294.png"
                                 class="infor-user-img-10" alt="">
                        @endif
                    </div>
                    <div class="col-9  ">
                        <div class="box-infor-user-10" style="margin-left: 25px;">
                            <div class="infor-user">
                                <div class="infor-user-10">
                                    <h3>{{ $user->full_name }}</h3>
                                    <div class="folow-user-20">
                                        <b>0</b> <span><a href="#" class="folow-user-20">folow</a></span>
                                    </div>
                                    <div class="folow-user-20">
                                        <b>0</b> <span><a href="#" class="folow-user-20">Your Folow</a></span>
                                    </div>
                                </div>

                                <div class="nut-folow-10" style="margin-top: 20px;">
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-outline-info" data-toggle="modal"
                                            data-target="#UserForm">
                                        Sửa thông tin
                                    </button>
                                    <button type="button" class="btn btn-outline-warning">+ Theo Dõi</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-1">
                        <i class="fas fa-star icon-user-100" style="color: orange;"></i>
                    </div>
                    <div class="col-11"><span class="font-write-user">Đánh giá :</span>
                        <span>{{ $user->averageRating }}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1">
                        <i class="far fa-calendar-times icon-user-100" style="color: black;"></i>
                    </div>
                    <div class="col-11"><span class="font-write-user"> Ngày tham gia :</span> <span>28/02/2020</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1">
                        <i class="fas fa-map-marker-alt icon-user-100" style="color: tomato;"></i>
                    </div>
                    <div class="col-11">
                        <span class="font-write-user"> Địa chỉ :</span>
                        <span> <?php
                            if ($address) {
                                echo $address->street . ' ' . $address->ward . ' ' . $address->district . ' ' . $address->province;
                            } else {
                                echo 'Chưa đăng ký';
                            }
                            ?>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1">
                        <i class="fas fa-comment-alt icon-user-100" style="color: #28a745;"></i>
                    </div>
                    <div class="col-11"><span class="font-write-user"> Phản Hồi Chat :</span> <span>Chưa có đánh giá</span></div>
                </div>
                <div class="row">
                    <div class="col-1">
                        <i class="fas fa-check-square icon-user-100" style="color: #007bff;"></i>
                    </div>
                    <div class="col-11"><span class="font-write-user"> Đã Cung Cấp :</span> <i class="fab fa-facebook"
                                                                                               style="margin-right:10px;font-size: 20px;color: #2e73ce;cursor: pointer;"
                                                                                               data-toggle="modal"
                                                                                               data-target="#exampleModal60"></i></i>
                        <div class="modal fade" id="exampleModal60" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Link facebook</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Link Facebook
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <i class="fas fa-map-marker-alt" style="margin-right:10px;font-size: 20px; color: tomato;"></i>
                        <i class="fas fa-envelope" style="margin-right:10px;font-size: 20px; color: orange;"></i>
                        <i class="fas fa-phone-square" style="margin-right:10px;font-size: 20px;"></i></div>
                </div>


            </div>
        </div>
    </div>

    <div class="container mt-3 mb-5" style="background-color: white;border-radius: 5px;width: 100%;height: 100%;">
        <div class="row">


            <div class="box-tab-user-10">

                <ul class="nav nav-pills mb-3 " id="pills-tab" role="tablist">
                    <li class="nav-item ">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                           aria-controls="pills-home" aria-selected="true">Sản phẩm đã bán</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
                           aria-controls="pills-profile" aria-selected="false">Sản phẩm đang bán</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                         aria-labelledby="pills-home-tab">
                        <!-- đã bán -->
                        <b>Đã bán</b> <span>- {{ count($soldProducts) }} sản phẩm</span>
                        <div class="gach-duoi-tag-buy"></div>
                        <div class="box-sold-buyers-10">
                            <div class="row" style="padding: 20px;">
                                <?php
                                foreach ($soldProducts as $product) {
                                    echo '
                                        <div class="col-md-2">
                                            <div class="box-new-product my-3">
                                                <div class="tag-a" style="display: block; cursor: text">
                                                    <a href="/san-pham/' . $product->id . '">
                                                        <img class="box-img-new-product"
                                                             src="https://bizweb.dktcdn.net/thumb/large/100/331/022/products/cc0d02d02201616ed43de5853b5a8b43.jpg?v=1572605540000"
                                                             alt="">
                                                        <div class="box-text-new-product">
                                                            <div class="box-text-name">
                                                                <div class="product-title">
                                                                    ' . $product->title . '
                                                                </div>
                                                            </div>
                                                            <div class="name-seller-newsp">
                                                                <div class="sdafsda"> ' . $user->full_name . ' </div>
                                                            </div>
                                                            <div class="box-duoc-tai-tro" style="background: gray; width: 80px;">
                                                                <span>Đã bán</span>
                                                            </div>
                                                            <div class="box-icon-product-10">
                                                                <div class="rating-stars">
                                                                    <div class="rating-stars__stars">
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star checked"></span>
                                                                        <span class="fa fa-star"></span>
                                                                        <span class="fa fa-star"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="product-box-footer ">Đã bán ' . count($soldProducts) . '</div>
                                                            </div>
                                                            <div class="box-text-price-product-10">
                                                                <div class="fixed-price d-inline-block ml-auto">' . number_format($product->price, 0, '.', ',') . '<span class="price-money">₫</span></div>
                                                            </div>

                                                        </div>
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                        ';
                                }
                                ?>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <!-- Đang bán -->
                        <div class="box-show-product-user-sales">
                            <b>Đang bán</b> <span>- {{ count($sellingProducts) }} sản phẩm</span>
                            <div class="gach-duoi-tag-buy"></div>

                            <?php
                            $now = time();
                            foreach ($sellingProducts as $product) {
                                $time = strtotime($product->created_at);
                                $datediff = $now - $time;
                                echo '
                                    <div class="buy-your-sellers-10">
                                        <div class="buy-your-sellers">
                                            <img style="width: 100%;height: 100% ;"
                                                 src="/storage/' . $product->avatar . '"
                                                 alt="">
                                        </div>

                                        <div class="sc-jlyJG gcGtIn"
                                             style="background-color: rgb(245, 154, 0);color: rgb(255, 255, 255);border-color: rgb(245, 154, 0);">
                                            HOT
                                        </div>
                                        <div class="arrow-up"></div>
                                        <div class="" style="position: absolute; bottom: 10px; right: 0px; z-index: 1;">
                                            <button
                                                id="btn_save_ad" class="sc-eqIVtm dHRRHy"
                                                style="background: white;border: none;"><img width="20"
                                                                                             src="https://static.chotot.com.vn/storage/adType/adItem/heart.png"
                                                                                             alt="like">
                                            </button>
                                        </div>
                                        <div class="box-text-user-buyer">
                                            <a href="/san-pham/' . $product->id . '" style="display: inline-block; width: 85%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">' . $product->title . '</a>
                                            <div class="price-user-buyer">
                                                <span>' . number_format($product->price, 0, '.', ',') . ' đ</span>
                                            </div>
                                            <div class="last-time-user">
                                                <i class="fas fa-shopping-bag color-grey-10"></i>
                                                <span class="color-grey-10" style="margin-left: 5px;">' . round($datediff / (60 * 60 * 24)) . ' ngày trước</span>

                                            </div>
                                        </div>
                                    </div>
                                    '; //end of echo
                            };
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection

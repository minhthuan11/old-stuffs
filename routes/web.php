<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//['middleware' => 'auth', 'uses' => 'FooController@index']);

Route::get('/', 'HomeController@showIndexPage');

Route::get('/tai-khoan/{id}', 'UserController@showIndexPage');

Route::group(['prefix' => 'danh-muc'], function () {
    Route::get('/{id?}', 'CategoryController@showIndexPage');
});

Route::group(['prefix' => 'profile'], function () {
    Route::get('/', 'AddressController@showProfilePage');
    Route::get('/sua-thong-tin', 'AddressController@showProfilePage');
    Route::post('/update', 'AddressController@updateUser');
    Route::get('/dia-chi/', 'AddressController@showIndexPage');
    Route::get('/dia-chi/create/{id?}', 'AddressController@showFormPage');
    Route::post('/dia-chi/create', 'AddressController@storeAddress');
    Route::post('/dia-chi/update', 'AddressController@updateAddress');
    Route::get('/dia-chi/{id}/destroy', 'AddressController@deleteAddress');
    Route::get('/san-pham', 'AddressController@showProductPage');
    Route::get('/san-pham/create/{id?}', 'AddressController@showProductForm');
    Route::post('/san-pham/create', 'AddressController@storeProduct');
    Route::post('/san-pham/update', 'AddressController@updateProduct');
    Route::get('/san-pham/{id}/destroy', 'AddressController@deleteProduct');
    Route::get('/san-pham/{id}/status', 'AddressController@changeSellingStatus');
});

Route::group(['prefix' => 'san-pham', 'as' => 'san-pham'], function () {
    Route::get('/{id}', 'ProductController@showIndexPage'); // S
});

Route::group(['prefix' => 'comment'], function () {
    Route::post('create', 'ProductController@storeCmt'); // S
});

Route::group(['prefix' => 'hinh-anh'], function () {
    Route::post('create', 'AddressController@storeImage'); // S
    Route::get('/{id}/delete', 'AddressController@deleteImage');
});


Route::get('register', 'Auth\RegisterController@getRegister');
Route::post('register', 'Auth\RegisterController@postRegister');

Route::get('login', [ 'as' => 'login', 'uses' => 'Auth\LoginController@getLogin']);
Route::post('login', [ 'as' => 'login', 'uses' => 'Auth\LoginController@postLogin']);

// Đăng xuất
Route::get('logout', [ 'as' => 'logout', 'uses' => 'Auth\LogoutController@getLogout']);
